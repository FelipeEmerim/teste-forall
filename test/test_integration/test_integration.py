"""Contain system integration tests."""

import os
from time import sleep

from assertpy import assert_that
from flask import url_for
import simplejson as json

from test.baseTest import BaseTest  # noqa


class IntegrationTests(BaseTest):

    """Contain integration tests for the main part of the system."""

    def test_run_address_finder_ok(self):
        """
        Test address finder running with a valid file.

        :return: None
        """
        self._run_address_finder_ok(self.SINGLE_ADDRESS_OK_FILE)

    def test_run_address_finder_ok_zip(self):
        """
        Test address finder running with a valid zip file.

        :return: None
        """
        self._run_address_finder_ok(self.SINGLE_ADDRESS_OK_FILE_ZIP)

    def test_run_address_finder_invalid_file_extension(self):
        """
        Test address finder running with an invalid extension file.

        :return: None
        """
        self._run_address_finder_error(self.RANDOM_PYTHON_FILE)

    def test_run_address_finder_invalid_file_extension_zip(self):
        """
        Test address finder running with an invalid extension file within a zipfile.

        :return: None
        """
        self._run_address_finder_error(self.RANDOM_PYTHON_FILE_ZIP)

    def test_run_address_finder_invalid_row_count_file(self):
        """
        Test address finder running with a file which row number is not a multiple of three.

        :return: None
        """
        self._run_address_finder_error(self.SINGLE_ADDRESS_MISSING_ONE_LINE)

    def test_run_address_finder_finished_with_invalid_coordinate_file(self):
        """
        Test address finder running with a file that contains an invalid coordinate.

        :return: None
        """
        self._run_address_finder_error(self.SINGLE_ADDRESS_ERROR_FILE)

    def _wait_until_job_finishes(self, job_url, interval=3, max_attempts=5):
        """
        Simulate a user making requests to the job/{job_id} endpoint until the job finishes.

        The job finishes when the status is neither created nor in progress. Should the maximum number of
        attempts be achieved, an error is raised.

        :param job_url: The url of the request to job status.
        :param interval: The interval between each attempt. Defaults to 3 seconds.
        :param max_attempts: The maximum number of attempts. Defaults to 5.

        :return: None
        """
        attempt = 0

        while True:
            response = self.client.get(job_url)

            response_dict = json.loads(response.get_data(as_text=True))

            if response_dict['status'] not in {'IN PROGRESS', 'CREATED'}:
                break

            if attempt >= max_attempts:
                raise Exception('Job limit time exceeded')

            attempt += 1
            sleep(interval)

    def _run_address_finder_error(self, file):
        """
        Run an address finder job expecting an error to happen.

        Contain every assertion needed to ensure the job resulted in error.

        :param file: The file to be submitted.

        :return: None
        """
        file_address = os.path.join(self.TEST_FILES_FOLDER, file)
        create_job_response = self._make_create_job_request(file_address)
        response_dict = json.loads(create_job_response.get_data(as_text=True))

        # Check if the job was created.
        assert_that(create_job_response.status_code).is_equal_to(201)
        self._wait_until_job_finishes(response_dict['status-link'])

        get_job_response = self.client.get(response_dict['status-link'])
        response_dict = json.loads(get_job_response.get_data(as_text=True))

        # Check if the status of the job is error.
        assert_that(response_dict['status']).is_equal_to('ERROR')

        get_file_ok_response = self.client.get(
            url_for('fileBlueprint.get_job_file', job_id=response_dict['id'], is_success=True),
        )

        # Check that no success file was generated
        assert_that(get_file_ok_response.status_code).is_equal_to(404)

        get_file_error = self.client.get(
            url_for('fileBlueprint.get_job_file', job_id=response_dict['id']),
        )

        # Check that no error file was generated
        assert_that(get_file_error.status_code).is_equal_to(404)

        # Check that there is no file in the upload folder of the app
        assert_that(len(os.listdir(self.app.config['UPLOAD_FOLDER']))).is_equal_to(0)

    def _run_address_finder_ok(self, file):
        """
        Run an address job expecting the result to be FINISHED.

        :param file: The file to be submitted.

        :return: None
        """
        file_address = os.path.join(self.TEST_FILES_FOLDER, file)

        create_job_response = self._make_create_job_request(file_address)

        response_dict = json.loads(create_job_response.get_data(as_text=True))

        # Check that the job was created.
        assert_that(create_job_response.status_code).is_equal_to(201)
        self._wait_until_job_finishes(response_dict['status-link'])

        get_job_response = self.client.get(response_dict['status-link'])
        response_dict = json.loads(get_job_response.get_data(as_text=True))

        # Check that the status is finished
        assert_that(response_dict['status']).is_equal_to('FINISHED')

        get_file_ok_response = self.client.get(
            url_for('fileBlueprint.get_job_file', job_id=response_dict['id'], is_success=True),
        )

        # Check that the address file was generated with the correct content.
        assert_that(get_file_ok_response.get_data(as_text=True)).contains_ignoring_case('santana')

        get_file_error = self.client.get(
            url_for('fileBlueprint.get_job_file', job_id=response_dict['id']),
        )

        # Check that no error file was generated
        assert_that(get_file_error.status_code).is_equal_to(404)

        # Check that there is no file in the upload folder of the app
        assert_that(len(os.listdir(self.app.config['UPLOAD_FOLDER']))).is_equal_to(0)

    def _make_create_job_request(self, file_address):
        """
        Make a request to the create job endpoint.

        :param file_address: The address of the file to be submitted.

        :return: The response of the request.
        """
        with open(file_address, 'rb') as file:
            create_job_response = self.client.post(
                url_for('jobBlueprint.create_job'),
                data={'file': file},
                content_type='multipart/form-data'
            )

        return create_job_response
