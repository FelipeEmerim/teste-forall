"""Contain file blueprints tests."""
from unittest import mock

from assertpy import assert_that
from flask import url_for

from main.model.AddressFile import AddressFile
from test.baseTest import BaseTest  # noqa:C0411
from test.helper import fileHelper  # noqa:C0411


class FileBlueprintTest(BaseTest):

    """File blueprint test case class."""

    @mock.patch('main.service.fileService.find_by_is_success_and_job_id', return_value=None)
    def test_get_job_file_not_found(self, find_method):
        """
        Test the get job file behavior when a file address is not found.

        :param find_method: Mock

        :return: None
        """
        response = self.client.get(
            url_for('fileBlueprint.get_job_file', job_id=1),
        )

        assert_that(response.status_code).is_equal_to(404)
        find_method.assert_called_with(False, 1)

    @mock.patch('main.service.fileService.find_by_is_success_and_job_id')
    def test_get_job_file(self, find_method):
        """
        Test the get_job_file method when the file is found.

        Since flask send from directory is unmockable we had to create a real file.

        :param find_method: Mock

        :return: None
        """
        addressFile = AddressFile(1, self.SINGLE_ADDRESS_OK_FILE, self.app.config['UPLOAD_FOLDER'], True)
        find_method.return_value = addressFile

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], self.SINGLE_ADDRESS_OK_FILE)

        response = self.client.get(
            url_for('fileBlueprint.get_job_file', job_id=1),
        )
        assert_that(find_method.called).is_true()

        assert_that(response.status_code).is_equal_to(200)
