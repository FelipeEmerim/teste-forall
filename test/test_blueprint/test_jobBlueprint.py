"""Contain tests related to the job blueprint."""

import io
from unittest import mock

from assertpy import assert_that
import simplejson as json
from flask import url_for

from test.baseTest import BaseTest  # noqa:C0411
from test.helper import jobHelper  # noqa:C0411


class JobBlueprintTest(BaseTest):

    """Job blueprint test case class."""

    @mock.patch('main.service.jobService.create_job', return_value=jobHelper.create_job())
    def test_create_job(self, create_job):
        """
        Test the behavior of the create job method.

        :param create_job: Mock

        :return: None
        """
        data = {'file': (io.BytesIO(b"this is a test"), 'test.txt')}

        response = self.client.post(
            url_for('jobBlueprint.create_job'),
            data=data,
            content_type='multipart/form-data'
        )

        response_dict = json.loads(response.get_data(as_text=True))

        assert_that(response.status_code).is_equal_to(201)
        assert_that(response_dict['success']).is_true()
        assert_that(response_dict['message']).is_equal_to("Job created successfully")
        assert_that(response_dict).contains_key("status-link")

        assert_that(create_job.called).is_true()

    @mock.patch('main.service.jobService.find_by_id', return_value=jobHelper.create_job())
    def test_job_details(self, find_by_id):
        """
        Test the behavior of the job details method when a job is found.

        :param find_by_id: Mock

        :return: None
        """
        response = self.client.get(
            url_for('jobBlueprint.job_details', job_id=1))

        response_dict = json.loads(response.get_data(as_text=True))

        assert_that(response.status_code).is_equal_to(200)
        assert_that(response_dict['message']).is_equal_to("dummy")
        assert_that(response_dict).contains_key("_links")
        assert_that(response_dict['name']).is_equal_to("dummy")
        find_by_id.assert_called_with(1)

    @mock.patch('main.service.jobService.find_by_id', return_value=None)
    def test_job_details_not_found(self, find_by_id):
        """
        Test the behavior of the job details method when a job is not found.

        :param find_by_id: Mock

        :return: None
        """
        response = self.client.get(
            url_for('jobBlueprint.job_details', job_id=1))

        assert_that(response.status_code).is_equal_to(404)
        find_by_id.assert_called_with(1)

    @mock.patch('main.service.jobService.find_all', return_value=[jobHelper.create_job(), jobHelper.create_job()])
    def test_jobs_details(self, find_all):
        """
        Test the behavior of the jobs_details method.

        :param find_all: Mock

        :return: None
        """
        response = self.client.get(
            url_for('jobBlueprint.jobs_details'))

        response_dict = json.loads(response.get_data(as_text=True))

        assert_that(response.status_code).is_equal_to(200)
        assert_that(find_all.called).is_true()

        assert_that(len(response_dict)).is_equal_to(2)
