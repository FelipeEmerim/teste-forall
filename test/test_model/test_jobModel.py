"""Contain tests related to the job model."""

from datetime import datetime

from assertpy import assert_that

from main.model.Job import Job
from test.baseTest import BaseTest  # noqa: C0411


class JobTest(BaseTest):

    """job model test case class."""

    def test_create_job(self):
        """
        Test that we can create a job.

        :return: None
        """
        job = Job()
        job.name = "dummy"
        self.db.session.add(job)
        self.db.session.commit()

        job = Job.query.first()

        assert_that(job).is_not_none()
        assert_that(job.name).is_equal_to("dummy")

    def test_job_init(self):
        """
        Test the init method of the job model.

        This is different from testing the __init__ method. This is a custom initializer.

        :return: None
        """
        job = Job()
        job.init("dummy", "dummy description", "dummy message")
        self.db.session.add(job)
        self.db.session.commit()

        job = Job.query.first()

        assert_that(job).is_not_none()
        assert_that(job.name).is_equal_to("dummy")
        assert_that(job.description).is_equal_to("dummy description")
        assert_that(job.message).is_equal_to("dummy message")

    @staticmethod
    def test_str_created_on():
        """
        Test the property str_created_on is working properly.

        :return: None
        """
        job = Job()

        now = datetime.now()
        date_string = now.strftime("%Y-%m-%d, %H:%M:%S")

        job.created_on = now

        assert_that(job.str_created_on).is_equal_to(date_string)

    @staticmethod
    def test_str_finished_on():
        """
        Test the property str_finished_on is working properly.

        :return: None
        """
        job = Job()

        now = datetime.now()
        date_string = now.strftime("%Y-%m-%d, %H:%M:%S")

        job.finished_on = now

        assert_that(job.str_finished_on).is_equal_to(date_string)

    @staticmethod
    def test_str_status():
        """
        Test the property str_status is working properly.

        :return: None
        """
        job = Job()
        job.status = 0
        assert_that(job.str_status).is_equal_to("CREATED")

        job.status = 1
        assert_that(job.str_status).is_equal_to("IN PROGRESS")

        job.status = 2
        assert_that(job.str_status).is_equal_to("ERROR")

        job.status = 3
        assert_that(job.str_status).is_equal_to("FINISHED")

        job.status = 4
        assert_that(job.str_status).is_equal_to("FINISHED WITH ERRORS")
