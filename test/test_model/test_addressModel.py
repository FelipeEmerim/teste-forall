"""Contain tests related to the address model."""

from assertpy import assert_that

from main.model.Address import Address, TempAddress
from test.baseTest import BaseTest  # noqa: C0411


class AddressTest(BaseTest):

    """Address model test case class."""

    def test_create_address(self):
        """
        Test that we can successfully create an address.

        :return: None
        """
        address = Address(latitude=123, longitude=456)
        self.db.session.add(address)
        self.db.session.commit()

        address = Address.query.first()

        assert_that(address).is_not_none()
        assert_that(address.latitude).is_equal_to(123)
        assert_that(address.longitude).is_equal_to(456)

    @staticmethod
    def test_address_mapper():
        """
        Test that the address mapper is working properly.

        This maps gmaps attributes to our address attributes.

        :return: None
        """
        address_types = {
            'street_number': 'street_number',
            'route': 'street_address',
            'sublocality_level_1': 'neighborhood',
            'administrative_area_level_2': 'city',
            'administrative_area_level_1': 'state',
            'country': 'country',
            'postal_code': 'postal_code',
        }

        for key, val in address_types.items():
            assert_that(Address._address_mapper(key)).is_equal_to(val)  # noqa: W0212

    @staticmethod
    def test_get_address_types():
        """
        Test that the get address types method is working properly.

        We use this to parse gmaps response.

        :return: None
        """
        values = [
            {'type': 'street_number', 'property': 'long_name'},
            {'type': 'route', 'property': 'long_name'},
            {'type': 'sublocality_level_1', 'property': 'long_name'},
            {'type': 'administrative_area_level_2', 'property': 'long_name'},
            {'type': 'administrative_area_level_1', 'property': 'short_name'},
            {'type': 'country', 'property': 'long_name'},
            {'type': 'postal_code', 'property': 'long_name'}
        ]

        for value in values:
            assert_that(Address.get_address_types()).contains(value)

    @staticmethod
    def test_set_attribute():
        """
        Test that we can set an attribute in our object given a gmaps attribute and value.

        :return: None
        """
        address = Address(latitude=123, longitude=456)
        address.set_attribute("administrative_area_level_2", "lalala")

        assert_that(address.city).is_equal_to("lalala")

    @staticmethod
    def test_copy_address():
        """
        Test that we can copy a real address into a temp address.

        :return: None
        """
        address = Address(latitude=123, longitude=456)
        temp_address = TempAddress()
        temp_address.copy_address(address)

        assert_that(temp_address.latitude).is_equal_to(address.latitude)
        assert_that(temp_address.longitude).is_equal_to(address.longitude)

    @staticmethod
    def test_get_export_columns():
        """
        Test that the export columns method is working properly.

        :return: None
        """
        expected_columns = [
            TempAddress.latitude.name,
            TempAddress.longitude.name,
            TempAddress.street_address.name,
            TempAddress.street_number.name,
            TempAddress.neighborhood.name,
            TempAddress.city.name,
            TempAddress.postal_code.name,
            TempAddress.state.name,
            TempAddress.country.name
        ]

        assert_that(TempAddress.get_export_columns()).is_equal_to(expected_columns)
