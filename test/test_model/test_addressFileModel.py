"""Contain tests related to the address file model."""

from assertpy import assert_that

from main.model.AddressFile import AddressFile
from main.model.Job import Job
from test.baseTest import BaseTest  # noqa: C0411


class AddressFileTest(BaseTest):

    """Address file model test case class."""

    def test_create_address_file(self):
        """
        Test that we can successfully create an address file.

        :return: None
        """
        job = Job()
        self.db.session.add(job)
        self.db.session.commit()

        address_file = AddressFile(
            job_id=job.id,
            filename="dummy",
            directory="dummy",
            is_success=True
        )

        self.db.session.add(address_file)
        self.db.session.commit()

        address_file = AddressFile.query.first()

        assert_that(address_file).is_not_none()
        assert_that(address_file.filename).is_equal_to("dummy")
        assert_that(address_file.is_success).is_equal_to(True)
