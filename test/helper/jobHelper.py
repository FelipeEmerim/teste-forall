"""Contain helper test methods related to job."""

from datetime import datetime

from main.model.Job import Job


def create_job(name="dummy", now=None, description="dummy", status=1, message="dummy"):
    """
    Create a dummy job object.

    :param name: The name of the job
    :param now: The dates of the job
    :param description: The description of the job
    :param status: The status of the job
    :param message: The message of the job

    :return: A main.model.Job.Job instance
    """
    if now is None:
        now = datetime.now()

    job = Job()
    job.id = 1
    job.init(name, description, message)
    job.status = status
    job.created_on = now
    job.finished_on = now

    return job
