"""Contain helper test methods related to address."""
from main.model.Address import Address


def create_address():
    """
    Create a dummy address to be used in tests.

    :return: model.Address.Address instance.
    """
    address = Address(longitude=123, latitude=456)
    address.city = "canoas"
    address.state = "RS"
    address.found = True
    address.postal_code = "123456"
    address.country = "Brasil"
    address.neighborhood = "Santana"
    address.street_address = "Rua 445"
    address.street_number = "999"

    return address


def get_dummy_gmaps_response():
    """
    Create a fake gmaps response to minimize the number of actual request.

    :return: dict containing the fake response.
    """
    return [
        {'address_components': [
            {'long_name': '405',
             'short_name': '405',
             'types': ['street_number']},

            {'long_name': 'Rua Monsenhor Veras',
             'short_name': 'R. Monsenhor Veras',
             'types': ['route']},

            {'long_name': 'Santana',
             'short_name': 'Santana',
             'types': ['political', 'sublocality', 'sublocality_level_1']},

            {'long_name': 'Porto Alegre',
             'short_name': 'Porto Alegre',
             'types': ['administrative_area_level_2', 'political']},

            {'long_name': 'Rio Grande do Sul',
             'short_name': 'RS',
             'types': ['administrative_area_level_1', 'political']},

            {'long_name': 'Brazil',
             'short_name': 'BR',
             'types': ['country', 'political']},

            {'long_name': '90610-010',
             'short_name': '90610-010',
             'types': ['postal_code']}
        ],
         'formatted_address': 'R. Monsenhor Veras, 405 - Santana, Porto Alegre - RS, 90610-010, Brazil',
         'geometry': {'location': {'lat': -30.049917, 'lng': -51.201439},
                      'location_type': 'ROOFTOP',
                      'viewport':
                          {'northeast': {'lat': -30.0485680197085, 'lng': -51.2000900197085},
                           'southwest': {'lat': -30.0512659802915, 'lng': -51.2027879802915}
                           }
                      },
         'place_id': 'ChIJ7Sk2a0B4GZURCoggMA5hFsw',
         'plus_code': {'compound_code': 'XQ2X+2C Porto Alegre, State of Rio Grande do Sul, Brazil',
                       'global_code': '48XCXQ2X+2C'},
         'types': ['street_address']}
    ]
