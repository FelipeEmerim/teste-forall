"""Contain helper test methods related to file."""

import os
from shutil import copyfile

from main.model.AddressFile import AddressFile
from main.model.Job import Job


def copy_file(src, dst, filename):
    """
    Copy a file from a source to a destination.

    :param src: The source folder.
    :param dst: The destination folder.
    :param filename: The name of the file.

    :return: None
    """
    source = os.path.join(src, filename)
    destination = os.path.join(dst, filename)

    copyfile(source, destination)


def create_address_file(job=None):
    """
    Create a dummy address file object.

    :param job: The job to link the address file to.

    :return: main.model.AddressFile.AddressFile instance
    """
    if not job:
        job = Job()
        job.id = 1

    file = AddressFile(job.id, "sadas", "sadas", True)
    return file


def get_dummy_next_three_lines():
    """
    Get fake next three lines return.

    Useful to avoid unneeded file handling.

    :return: A list of lists containing the values.
    """
    return [
        [
            'Latitude: 30°02′59″S   -30.04982864',
            'Longitude: 51°12′05″W   -51.20150245',
            'Distance: 2.2959 km  Bearing: 137.352°'
        ]
    ]
