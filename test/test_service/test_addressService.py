"""Contain unit tests related to the address service."""

import os
from unittest import mock

from assertpy import assert_that
from sqlalchemy import and_

from main.model.Address import Address, TempAddress
from test.baseTest import BaseTest  # noqa:C0411
from test.helper import fileHelper, addressHelper  # noqa:C0411
from main.service import addressService


class AddressServiceTest(BaseTest):

    """
    AddressService test case class.

    In cases where the address service is mocked is to avoid making unnecessary request to the \
    google api, the only method that should make such request is the reverse_geocode method \
    which has this purpose.
    """

    @mock.patch('main.service.addressService.find_address', return_value=TempAddress())
    @mock.patch('main.service.fileService.iterate_n_lines_at_a_time',
                return_value=iter(fileHelper.get_dummy_next_three_lines()))
    def test_find_addresses(self, mock1, file_mock):
        """
        Test the behavior of the find addresses method.

        :param mock1: Mock
        :param file_mock: Mock

        :return: None
        """
        assert_that(Address.query.first()).is_none()

        filename = self.SINGLE_ADDRESS_OK_FILE_ZIP
        file_address = os.path.join(self.app.config['UPLOAD_FOLDER'], filename)

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], filename)

        addressService.find_addresses(file_address)
        assert_that(mock1.called).is_true()
        assert_that(file_mock.called).is_true()
        assert_that(self.db.session.new).is_true()

    @staticmethod
    def test_validate_coordinate_valid():
        """
        Test the validate coordinate method against a valid coordinate.

        In this case everything should run smoothly.

        :return: None
        """
        addressService.validate_coordinate("123456789123", 3)

    @staticmethod
    def test_validate_coordinate_not_number():
        """
        Test validate coordinate method against a coordinate that is not a number.

        :return: None
        """
        assert_that(addressService.validate_coordinate).raises(ValueError).when_called_with("12av", 2)

    @staticmethod
    def test_validate_coordinate_invalid_length():
        """
        Test the validate coordinate method against invalid length coordinates.

        This is important because the length of a coordinate indicates its precision.

        :return: None
        """
        assert_that(addressService.validate_coordinate).raises(ValueError).when_called_with("1234", 3)
        assert_that(addressService.validate_coordinate).raises(ValueError).when_called_with("12342343241234324234", 3)

    # mock to avoid making actual requests to google as much as possible
    @mock.patch('main.service.addressService.get_address_from_db', return_value=True)
    @mock.patch('main.service.addressService.reverse_geocode', return_value=True)
    def test_find_address_in_db(self, mock1, mock2):  # noqa:R0201
        """
        Test the behavior of the find address method when we have it in the database.

        :param mock1: Mock
        :param mock2: Mock
        :return:
        """
        addressService.find_address(23, 23)
        assert_that(mock2.called).is_true()
        assert_that(mock1.called).is_false()

    # mock to avoid making actual requests to google as much as possible
    @mock.patch('main.service.addressService.get_address_from_db', return_value=None)
    @mock.patch('main.service.addressService.reverse_geocode', return_value=True)
    def test_find_address_not_in_db(self, mock1, mock2):  # noqa:R0201
        """
        Test the behavior of the find address method when we dont have it in the database.

        :param mock1: Mock
        :param mock2: Mock
        :return:
        """
        addressService.find_address(23, 23)
        assert_that(mock1.called).is_true()
        assert_that(mock2.called).is_true()

    @staticmethod
    def test_reverse_geocode():
        """
        Test that given a latitude and a longitude we can create a temp address object.

        It is also important to save the address to the global table to reuse it.

        :return: None
        """
        temp_address = addressService.reverse_geocode(lat=-30.04982864, lng=-51.20150245)

        address = Address.query.filter(and_(
            Address.latitude == -30.04982864, Address.longitude == -51.20150245)).first()

        assert_that(address).is_not_none()

        assert_that(temp_address).is_not_none()
        assert_that(temp_address.neighborhood).is_equal_to_ignoring_case("santana")
        assert_that(temp_address.state).is_equal_to("RS")

    @staticmethod
    def test_parse_reverse_geocode_response_empty_response():
        """
        Test that when we get an empty response from the api the application marks the address as not found.

        :return: None
        """
        address = Address(latitude=123, longitude=456)
        address = addressService.parse_reverse_geocode_response(address, [])
        assert_that(address.found).is_false()

    @staticmethod
    def test_parse_reverse_geocode_response():
        """
        Test that we can map an map api response to an address object.

        :return: None
        """
        address = Address(latitude=123, longitude=456)
        address = addressService.parse_reverse_geocode_response(address, addressHelper.get_dummy_gmaps_response())
        assert_that(address.found).is_true()
        assert_that(address.state).is_equal_to("RS")

    @staticmethod
    def test_get_type_value_from_request():
        """
        Test that we can get an address component and the desired property from the map api response.

        :return: None
        """
        address_type = {'type': 'administrative_area_level_1', 'property': 'short_name'}

        value = addressService.get_type_value_from_request(address_type,
                                                           addressHelper.get_dummy_gmaps_response())

        assert_that(value).is_equal_to("RS")

    def test_get_address_from_db(self):
        """
        Test the get address from db method.

        This searches the global table for an address. If it find one, it maps the address to a temp address \
        and returns it.

        :return: None
        """
        address = Address(latitude=1, longitude=2)
        address.city = "tangamandápio"
        self.db.session.add(address)
        self.db.session.commit()

        temp_address = addressService.get_address_from_db(1, 2)
        assert_that(temp_address.city).is_equal_to(address.city)
        assert_that(temp_address.latitude).is_equal_to(address.latitude)
        assert_that(temp_address.longitude).is_equal_to(address.longitude)

    @staticmethod
    def test_get_address_from_db_not_found():
        """
        Test that when an address is not in the database, none is returned.

        :return: None
        """
        temp_address = addressService.get_address_from_db(1, 2)
        assert_that(temp_address).is_none()
