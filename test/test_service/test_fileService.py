"""Contain unit tests related to the address service."""

import os
import zipfile
from unittest import mock

from assertpy import assert_that
from werkzeug.datastructures import FileStorage

from main.model.Address import TempAddress
from main.model.AddressFile import AddressFile
from main.service import fileService
from test.baseTest import BaseTest  # noqa:C0411
from test.helper import fileHelper, addressHelper, jobHelper  #noqa: C0411


class FileServiceTest(BaseTest):

    """FileService test case class."""

    def test_save_file(self):
        """
        Test the method that get a werkzeug file storage object and save it to the app's upload folder.

        :return: None
        """
        with open(os.path.join(self.TEST_FILES_FOLDER, self.SINGLE_ADDRESS_OK_FILE), 'rb') as file:

            file = FileStorage(file)
            filename = fileService.save_file(file)

        file_path = os.path.join(self.app.config['UPLOAD_FOLDER'], filename)

        assert_that(os.path.exists(file_path)).is_true()

    def test_save_file_empty_name(self):
        """
        Empty name files mean empty files and should not be saved.

        :return: None
        """
        with open(os.path.join(self.TEST_FILES_FOLDER, self.SINGLE_ADDRESS_OK_FILE), 'rb') as file:

            file = FileStorage(file)
            file.filename = ''
            assert_that(fileService.save_file).raises(ValueError).when_called_with(file)

        assert_that(len(os.listdir(self.app.config['UPLOAD_FOLDER']))).is_equal_to(0)

    def test_handle_zip_file(self):
        """
        Test the success flow of zip file handling.

        :return: None
        """
        file_address = os.path.join(self.app.config['UPLOAD_FOLDER'], self.SINGLE_ADDRESS_OK_FILE_ZIP)

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], self.SINGLE_ADDRESS_OK_FILE_ZIP)

        with zipfile.ZipFile(file_address) as zfile:
            file_address = fileService.handle_zip_files(zfile, file_address, self.app.config['UPLOAD_FOLDER'])
            assert_that(file_address).is_not_none()

            assert_that(os.path.exists(file_address)).is_true()

    def test_handle_zip_file_with_multiple_files(self):
        """
        Test the case where a zip file contains multiple files, none should be save or kept.

        :return: None
        """
        file_address = os.path.join(self.app.config['UPLOAD_FOLDER'], self.MORE_THAN_ONE_FILE_ZIP)

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], self.MORE_THAN_ONE_FILE_ZIP)

        with zipfile.ZipFile(file_address) as zfile:
            file_address = fileService.handle_zip_files(zfile, file_address, self.app.config['UPLOAD_FOLDER'])
            assert_that(file_address).is_none()

            assert_that(len(os.listdir(self.app.config['UPLOAD_FOLDER']))).is_equal_to(0)

    def test_process_file_valid_file(self):
        """
        Test the method that do basic validation and processing on a file that was just saved.

        :return: None
        """
        filename = self.SINGLE_ADDRESS_OK_FILE

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], self.SINGLE_ADDRESS_OK_FILE)

        file_address = fileService.process_file(filename, self.app.config['UPLOAD_FOLDER'])

        assert_that(file_address).is_not_none()

        assert_that(os.path.exists(file_address)).is_true()

    def test_process_file_valid_zip_file(self):
        """
        Test the flow of processing a valid zipFile.

        :return: None
        """
        filename = self.SINGLE_ADDRESS_OK_FILE_ZIP

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], self.SINGLE_ADDRESS_OK_FILE_ZIP)

        file_address = fileService.process_file(filename, self.app.config['UPLOAD_FOLDER'])

        assert_that(file_address).is_not_none()

        assert_that(os.path.exists(file_address)).is_true()

    def test_process_file_invalid_file(self):
        """
        Test the process file method against an invalid file.

        :return: None
        """
        filename = self.RANDOM_PYTHON_FILE

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], self.RANDOM_PYTHON_FILE)

        file_address = fileService.process_file(filename, self.app.config['UPLOAD_FOLDER'])

        assert_that(file_address).is_none()

        assert_that(len(os.listdir(self.app.config['UPLOAD_FOLDER']))).is_equal_to(0)

    def test_process_file_invalid_zip_file(self):
        """
        Test the process file method against an invalid zip file.

        :return: None
        """
        filename = self.RANDOM_PYTHON_FILE_ZIP

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], self.RANDOM_PYTHON_FILE_ZIP)

        file_address = fileService.process_file(filename, self.app.config['UPLOAD_FOLDER'])

        assert_that(file_address).is_none()

        assert_that(len(os.listdir(self.app.config['UPLOAD_FOLDER']))).is_equal_to(0)

    @mock.patch('main.service.addressService.validate_coordinate')
    def test_validate_file_valid_file(self, _):
        """
        Test the validate file against a valid file.

        :param _: Mock

        :return: None
        """
        filename = self.SINGLE_ADDRESS_OK_FILE
        file_address = os.path.join(self.app.config['UPLOAD_FOLDER'], filename)

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], filename)

        # should run smoothly
        fileService.validate_file(file_address)

    @mock.patch('main.service.addressService.validate_coordinate', side_effect=ValueError)
    def test_validate_file_invalid_coordinate_file(self, _):
        """
        Test the validate file against a file that contains an invalid coordinate.

        :param _: Mock
        :return: None
        """
        filename = self.SINGLE_ADDRESS_ERROR_FILE
        file_address = os.path.join(self.app.config['UPLOAD_FOLDER'], filename)

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], filename)

        # should run smoothly
        assert_that(fileService.validate_file).raises(ValueError).when_called_with(file_address)

    @mock.patch('main.service.addressService.validate_coordinate')
    def test_validate_file_invalid_row_count(self, _):
        """
        Test the validate file method against a file that has an invalid row count.

        Row count not multiple of three.

        :param _: Mock

        :return: None
        """
        filename = self.SINGLE_ADDRESS_MISSING_ONE_LINE
        file_address = os.path.join(self.app.config['UPLOAD_FOLDER'], filename)

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], filename)

        # should run smoothly
        assert_that(fileService.validate_file).raises(ValueError).when_called_with(file_address)

    def test_iterate_n_lines_at_a_time(self):
        """
        Test the function that iterate n lines at a time on a file.

        :return: None
        """
        filename = self.SINGLE_ADDRESS_OK_FILE
        file_address = os.path.join(self.app.config['UPLOAD_FOLDER'], filename)

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], filename)

        with open(file_address, 'r', encoding='utf-8', newline='') as file:

            generator = fileService.iterate_n_lines_at_a_time(file)

            rows = next(generator)

            assert_that(len(rows)).is_equal_to(3)

            assert_that(next).raises(StopIteration).when_called_with(generator)

    def test_strip_newlines(self):
        """
        Test the method that removes newline characters from a file.

        :return: None
        """
        filename = self.SINGLE_ADDRESS_OK_FILE
        file_address = os.path.join(self.app.config['UPLOAD_FOLDER'], filename)

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], filename)

        with open(file_address, 'r', encoding='utf-8', newline='') as file:
            generator = fileService.iterate_n_lines_at_a_time(file)

            rows = next(generator)

            rows = fileService.strip_newlines(rows)

            [[assert_that(i).does_not_contain(os.linesep) for i in r] for r in rows]  # noqa:W0106

    @staticmethod
    def test_strip_newlines_none_type():
        """
        Test the strip newlines method against a none type object.

        :return: None
        """
        rows = fileService.strip_newlines()
        assert_that(len(rows)).is_equal_to(0)

    def test_read_next_n_lines(self):
        """
        Test the read next n lines function.

        :return: None
        """
        filename = self.SINGLE_ADDRESS_OK_FILE
        file_address = os.path.join(self.app.config['UPLOAD_FOLDER'], filename)

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], filename)

        with open(file_address, 'r', encoding='utf-8', newline='') as file:
            rows = fileService.read_next_n_lines(file, 3)

            assert_that(len(rows)).is_equal_to(3)

    def test_count_rows(self):
        """
        Test the function that counts rows of a file.

        :return: None
        """
        filename = self.SINGLE_ADDRESS_OK_FILE
        file_address = os.path.join(self.app.config['UPLOAD_FOLDER'], filename)

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], filename)

        with open(file_address, 'r', encoding='utf-8', newline='') as file:
            count = fileService.count_rows(file)

            assert_that(count).is_equal_to(3)

    def test_export_address_file(self):
        """
        Test the function that export the address file to the default folder.

        :return: None
        """
        address = addressHelper.create_address()
        temp_address = TempAddress()
        temp_address.copy_address(address)
        job = jobHelper.create_job()
        self.db.session.add(job)

        self.db.session.commit()

        TempAddress.__table__.drop(self.db.session.connection(), checkfirst=True)
        TempAddress.__table__.create(self.db.session.connection())
        self.db.session.add(address)

        self.db.session.add(temp_address)

        fileService.export_address_file(True, job, TempAddress.get_export_columns())

        assert_that(len(os.listdir(self.app.config['EXPORT_FOLDER']))).is_equal_to(1)

        file = AddressFile.query.first()

        with open(os.path.join(file.directory, file.filename), 'r', newline='') as file:
            content = file.readlines()

            assert_that(content[1]).contains("canoas")

        TempAddress.__table__.drop(self.db.session.connection())

    def test_export_address_file_defined_folder(self):
        """
        Test the function that exports an address file, but this time the folder parameter is passed.

        :return: None
        """
        address = addressHelper.create_address()
        temp_address = TempAddress()
        temp_address.copy_address(address)
        job = jobHelper.create_job()
        self.db.session.add(job)

        self.db.session.commit()

        TempAddress.__table__.drop(self.db.session.connection(), checkfirst=True)
        TempAddress.__table__.create(self.db.session.connection())
        self.db.session.add(address)

        self.db.session.add(temp_address)

        fileService.export_address_file(True,
                                        job,
                                        TempAddress.get_export_columns(),
                                        folder=self.app.config['EXPORT_FOLDER'])

        assert_that(len(os.listdir(self.app.config['EXPORT_FOLDER']))).is_equal_to(1)

        file = AddressFile.query.first()

        with open(os.path.join(file.directory, file.filename), 'r', newline='') as file:
            content = file.readlines()

            assert_that(content[1]).contains("canoas")

        TempAddress.__table__.drop(self.db.session.connection())

    def test_find_by_is_success_and_job_id(self):
        """
        Test the find address file method.

        :return: None
        """
        job = jobHelper.create_job()
        self.db.session.add(job)
        self.db.session.commit()

        self.db.session.add(fileHelper.create_address_file(job))
        self.db.session.commit()

        file = fileService.find_by_is_success_and_job_id(True, job.id)

        assert_that(file).is_not_none()
        assert_that(file.job_id).is_equal_to(job.id)

    def test_remove_file(self):
        """
        Test the remove file method.

        :return: None
        """
        filename = self.SINGLE_ADDRESS_OK_FILE
        file_address = os.path.join(self.app.config['UPLOAD_FOLDER'], filename)

        fileHelper.copy_file(self.TEST_FILES_FOLDER, self.app.config['UPLOAD_FOLDER'], filename)

        fileService.remove_file(file_address)

        assert_that(len(os.listdir(self.app.config['UPLOAD_FOLDER']))).is_equal_to(0)
