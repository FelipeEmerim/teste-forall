"""Contain unit tests related to job service."""

from unittest import mock

from assertpy import assert_that

from main.model.Address import TempAddress
from main.model.Job import Job
from main.service import jobService
from test.baseTest import BaseTest  # noqa:C0411


class JobServiceTest(BaseTest):

    """job service test case class."""

    @mock.patch('main.service.jobService.address_finder_task.delay')
    @mock.patch('main.service.fileService.save_file')
    def test_create_job(self, file_mock, task_mock):  # noqa:R0201
        """
        Test if the job is being created.

        Here we can use a string as the file since its value will not be used.

        :return: None
        """
        job = jobService.create_job("dummy")
        assert_that(job.status).is_equal_to(0)
        assert_that(job.name).is_equal_to('Address finder')
        assert_that(job.message).is_equal_to("job created")

        file_mock.assert_called_once_with("dummy")
        assert_that(task_mock.called).is_true()

    def test_finish_job(self):
        """
        Test that a job is being properly finished.

        :return: None
        """
        job = Job()
        job.name = "dummy"
        self.db.session.add(job)
        self.db.session.commit()

        jobService.finish_job(job, 3, "dummy message")

        assert_that(job.status).is_equal_to(3)
        assert_that(job.message).is_equal_to("dummy message")

    def test_find_by_id(self):
        """
        Test that given a job id, the respective job is found.

        :return: None
        """
        job = Job()
        job.name = "dummy"
        self.db.session.add(job)
        self.db.session.commit()

        job = jobService.find_by_id(job.id)

        assert_that(job.name).is_equal_to("dummy")

    @mock.patch('main.service.fileService.validate_file')
    @mock.patch('main.service.fileService.process_file', return_value="nice")
    @mock.patch('main.service.fileService.remove_file')
    @mock.patch('main.service.addressService.find_addresses')
    def test_address_finder(self, find_addresses, remove_file, process_file, validate_file):
        """
        Check the behavior of the address_finder method in a successful situation.

        We can pass strings to all mock parameters because they wont do anything, we just need to make sure \
        they are called correctly.

        :param find_addresses: Mock
        :param remove_file: Mock
        :param process_file: Mock
        :param validate_file: Mock

        :return: None
        """
        job = Job()
        self.db.session.add(job)
        self.db.session.commit()

        jobService.address_finder(job.id, "dummy", "dummy")

        find_addresses.assert_called_with("nice")
        validate_file.assert_called_with("nice")
        remove_file.assert_called_with("nice")
        process_file.assert_called_with("dummy", "dummy")

    @mock.patch('main.service.fileService.validate_file')
    @mock.patch('main.service.fileService.process_file', return_value=None)
    @mock.patch('main.service.fileService.remove_file')
    @mock.patch('main.service.addressService.find_addresses')
    def test_address_finder_invalid_file_format(self, find_addresses, remove_file, process_file, validate_file):
        """
        Check the behavior of the address finder method in case of an invalid file format.

        This emulates the scenario where a file is invalid.

        :param find_addresses: Mock
        :param remove_file: Mock
        :param process_file: Mock
        :param validate_file: Mock

        :return: None
        """
        job = Job()
        self.db.session.add(job)
        self.db.session.commit()

        jobService.address_finder(job.id, "dummy", "dummy")

        process_file.assert_called_with("dummy", "dummy")

        assert_that(job.status).is_equal_to(2)
        assert_that(job.message).is_equal_to('Invalid file format supplied')

        assert_that(find_addresses.called).is_false()
        assert_that(validate_file.called).is_false()
        assert_that(remove_file.called).is_false()

    @mock.patch('main.service.fileService.validate_file', side_effect=ValueError('lalala'))
    @mock.patch('main.service.fileService.process_file', return_value="nice")
    @mock.patch('main.service.fileService.remove_file')
    @mock.patch('main.service.addressService.find_addresses')
    def test_address_finder_invalid_file(self, find_addresses, remove_file, process_file, validate_file):
        """
        Test the behavior of the address finder in case the contents of a file are invalid.

        :param find_addresses: Mock
        :param remove_file: Mock
        :param process_file: Mock
        :param validate_file: Mock

        :return: None
        """
        job = Job()
        self.db.session.add(job)
        self.db.session.commit()

        jobService.address_finder(job.id, "dummy", "dummy")

        process_file.assert_called_with("dummy", "dummy")
        validate_file.assert_called_with("nice")

        assert_that(job.status).is_equal_to(2)
        assert_that(job.message).is_equal_to('lalala')

        assert_that(find_addresses.called).is_false()
        assert_that(remove_file.called).is_true()

    @mock.patch('main.service.fileService.validate_file')
    @mock.patch('main.service.fileService.process_file', return_value="nice")
    @mock.patch('main.service.fileService.remove_file')
    @mock.patch('main.service.addressService.find_addresses', side_effect=Exception)
    def test_address_finder_generic_error(self, find_addresses, remove_file, process_file, validate_file):
        """
        Test the address finder job generic errror handler, for internal server errors.

        :param find_addresses: Mock
        :param remove_file: Mock
        :param process_file: Mock
        :param validate_file: Mock

        :return: None
        """
        job = Job()
        self.db.session.add(job)
        self.db.session.commit()

        jobService.address_finder(job.id, "dummy", "dummy")

        process_file.assert_called_with("dummy", "dummy")
        validate_file.assert_called_with("nice")

        assert_that(job.status).is_equal_to(2)
        assert_that(job.message).is_equal_to('An error occurred during file processing')

        assert_that(find_addresses.called).is_true()
        assert_that(remove_file.called).is_true()

    @mock.patch('main.service.fileService.export_address_file')
    def test_finish_success_job_with_errors(self, export_address_file):
        """
        Test the behavior of the finish success job method in case of errors.

        In this case the job runs successfully but some addresses cannot be found,
        so a different action must happen.

        :param export_address_file: Mock

        :return: None
        """
        job = Job()
        temp_address = TempAddress()
        temp_address.found = False

        self.db.session.add(temp_address)
        self.db.session.add(job)
        self.db.session.commit()

        jobService.finish_success_job(job)

        self.db.session.commit()

        assert_that(job.status).is_equal_to(4)
        assert_that(job.message).is_equal_to('Some addresses could not be found')

        assert_that(export_address_file.called).is_true()

    @mock.patch('main.service.fileService.export_address_file')
    def test_finish_success_job_no_errors(self, export_address_file):
        """
        Test the behavior of the finish success job method in case all went good.

        :param export_address_file: Mock

        :return: None
        """
        job = Job()
        temp_address = TempAddress()
        temp_address.found = True

        self.db.session.add(temp_address)
        self.db.session.add(job)
        self.db.session.commit()

        jobService.finish_success_job(job)

        self.db.session.commit()

        assert_that(job.status).is_equal_to(3)
        assert_that(job.message).is_equal_to('job finished successfully')

        assert_that(export_address_file.called).is_true()

    def test_find_all(self):
        """
        Test the method find all.

        The find all method should return all jobs from the database.

        :return: None
        """
        job = Job()
        job.name = "dummy"
        self.db.session.add(job)
        self.db.session.commit()

        jobs = jobService.find_all()

        assert_that(len(jobs)).is_equal_to(1)
        assert_that(jobs[0].name).is_equal_to("dummy")
