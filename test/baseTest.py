"""Define default testing behaviors."""

import os
import shutil
import unittest

from main import create_app, db


class BaseTest(unittest.TestCase):

    """Contain common behaviors for every test."""

    TEST_FILES_FOLDER = 'test/test-files'
    SINGLE_ADDRESS_OK_FILE = 'single_address_ok.txt'
    SINGLE_ADDRESS_OK_FILE_ZIP = 'single_address_ok.txt.zip'
    MORE_THAN_ONE_FILE_ZIP = 'more_than_one_file.zip'
    SINGLE_ADDRESS_ERROR_FILE = 'single_address_error.txt'
    SINGLE_ADDRESS_ERROR_FILE_ZIP = 'single_address_error.txt.zip'
    RANDOM_PYTHON_FILE = 'random_python_file.py'
    RANDOM_PYTHON_FILE_ZIP = 'random_python_file.py.zip'
    SINGLE_ADDRESS_MISSING_ONE_LINE = 'single_address_missing_one_line.txt'

    def setUp(self) -> None:
        """
        Define setup for tests.

        Create app, database, folder and push app context to tests.

        :return: None
        """
        self.app = create_app()

        self.app_context = self.app.app_context()

        self.client = self.app.test_client()

        self.app_context.push()
        self.db = db

        self.db.create_all()

        if not os.path.exists(self.app.config['UPLOAD_FOLDER']):
            os.mkdir(self.app.config['UPLOAD_FOLDER'])

        if not os.path.exists(self.app.config['EXPORT_FOLDER']):
            os.mkdir(self.app.config['EXPORT_FOLDER'])

    def tearDown(self) -> None:
        """
        Define TearDown for tests.

        Drop the database, remove folders and app context.

        :return: None
        """
        self.db.session.remove()
        self.db.drop_all()
        self.app_context.pop()

        shutil.rmtree(self.app.config['UPLOAD_FOLDER'], ignore_errors=True)
        shutil.rmtree(self.app.config['EXPORT_FOLDER'], ignore_errors=True)
