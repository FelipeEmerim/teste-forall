"""Contain job schema tests."""

from assertpy import assert_that

from main.model.Job import Job
from main.schema.jobSchema import job_schema
from test.baseTest import BaseTest  # noqa:C0411
from test.helper.jobHelper import create_job  # noqa:C0411


class JobSchemaTest(BaseTest):

    """Job schema test case class."""

    def test_job_schema_dump(self):
        """
        Test the job schema dump method.

        :return: None
        """
        job = create_job(status=1)
        self.db.session.add(job)
        self.db.session.commit()

        job = Job.query.first()

        data = job_schema.dump(job).data
        assert_that(data).contains_key("status")
        assert_that(data['status']).is_equal_to("IN PROGRESS")
        assert_that(data).contains_key("name")
        assert_that(data["name"]).is_equal_to("dummy")

    def test_job_schema_dump_in_progress(self):
        """
        Check the job schema dump method for in progress jobs.

        Necessary because we have conditional links that are handled in custom logic.

        :return: None
        """
        job = create_job(status=1)
        self.db.session.add(job)
        self.db.session.commit()

        job = Job.query.first()

        data = job_schema.dump(job).data

        # should not contain links to files since job is in progress
        assert_that(data['_links']).does_not_contain_key("success_file")
        assert_that(data['_links']).does_not_contain_key("error_file")

    def test_job_schema_dump_finished(self):
        """
        Check the job schema dump method for finished jobs.

        Necessary because we have conditional links that are handled in custom logic.

        :return: None
        """
        job = create_job(status=3)
        self.db.session.add(job)
        self.db.session.commit()

        job = Job.query.first()

        data = job_schema.dump(job).data

        # should only contain success file link since there was no errors
        assert_that(data['_links']).contains_key("success_file")
        assert_that(data['_links']).does_not_contain_key("error_file")

    def test_job_schema_dump_finished_with_errors(self):
        """
        Check the job schema dump method for finished with errors jobs.

        Necessary because we have conditional links that are handled in custom logic.

        :return: None
        """
        job = create_job(status=4)
        self.db.session.add(job)
        self.db.session.commit()

        job = Job.query.first()

        data = job_schema.dump(job).data

        # should contain both keys since the job finished with errors
        assert_that(data['_links']).contains_key("success_file")
        assert_that(data['_links']).contains_key("error_file")
