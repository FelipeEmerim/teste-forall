# Documentação teste-forall

## Requirimentos

* Python 3.7.3 instalado
* Um Linux debian ou baseado.(testado no ubuntu 16.04 e 18.04)
* Os teste foram realizados utilizando o bash como terminal

## Uma forma de instalar o python

Para gerenciar as versões do python sem gerar conflitos no sistema podemos utilizar o Pyenv.
O pyenv deve ser instalado no usuário que irá rodar a aplicação.

### Instalando dependencias do Pyenv

Para instalar as dependências do pyenv e do python rodar:

```
sudo apt-get update

sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev git
```

### Baixando o instalador do Pyenv

Após instalar as dependências baixar e executar o instalador do pyenv:

```curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer  | bash ```

Após terminar a instalação do pyenv será necessário adicionar as três linhas mostradas no terminal para o final do arquivo bashrc para serem executadas sempre que o bash iniciar. Após isto reiniciar o terminal.

### Removendo o pyenv

Se precisar remover o pyenv basta deletar as linhas que adicionou do seu .bashrc e rodar:

```rm -fr ~/.pyenv```

## Instalando o python 3.7.3 através do Pyenv

Para instalar o python 3.7.3 precisamos usar os comandos.

```
pyenv update

pyenv install 3.7.3
```

Após isto temos o python 3.7.3 no ambiente. Após clonar a aplicação basta rodar o comando abaixo na pasta da aplicação:

```pyenv local 3.7.3```

Isto diz que o diretório da aplicação irá usar o python 3.7.3 mantendo inalterado o python do sistema. Ao rodar
o comando de ver a versão do python deve estar aplicada a versão 3.7.3, caso não esteja pode ser necessário reiniciar o terminal.

## Instalando o postgres 

Para instalar o postgres precisamos rodar: 

```
sudo apt-get update

sudo apt-get install postgresql postgresql-contrib
```

### Configurando uma database

Agora é preciso criar um usuário, uma database e garantir permissão nesta database a este usuário.

```
sudo -u postgres psql

CREATE DATABASE dev_testeforall;

CREATE USER dev_testeforall WITH PASSWORD 'dev_testeforall';

ALTER DATABASE dev_testeforall OWNER TO dev_testeforall;

GRANT ALL PRIVILEGES ON DATABASE "dev_testeforall" to dev_testeforall;

\q
```

## Instalando o redis

O Redis será responsável por servir como broker para as filas do Celery.
Para instalar o redis rodar.

```sudo apt-get install redis-server```

## Clonando o projeto

Rodar o comando: 
```git clone https://FelipeEmerim@bitbucket.org/FelipeEmerim/teste-forall.git```

Pode ser publicado tanto o master quanto a última tag disponível.

## Criando o ambiente virtual 

Para criar o ambiente virtual pode-se usar o comando:

```python -m venv .teste-forall```

## Adicionado as variáveis de ambiente

A aplicação requer algumas variáveis de ambiente para rodar. Isto é necessário para deixá-la configurável.

As variáveis são:

```
FLASK_ENV: descreve o tipo de ambiente do flask que estamos utilizando. Para o teste pode ser development

POSTGRES_URL: Host do postgres

POSTGRES_USER: Usuário do postgres

POSTGRES_PW: Senha do respectivo usuário.

POSTGRES_DB: Database que será utilizada pela aplicação.

POSTGRES_PORT: Em qual porta esta conexão com o postgres irá rodar. O padrão é "5432".

APP_SETTINGS: Informa a localização do arquivo de configuração do flask

SK_APP: Informa o arquivo principal do flask

TASK_QUEUE: Indica a fila de processos que o worker do celery deverá ouvir.

SERVER_NAME: Nome do servidor - para uso local deixar como localhost:5000

PREFERRED_URL_SCHEMA: colocar http para uso local

MAPS_API_KEY: chave de api do google maps

UPLOAD_FOLDER: Pasta de importação da aplicação. 

EXPORT_FOLDER: Pasta de exportação da aplicação

CELERY_BROKER_URL: Url do broker do celery

TESTING: Indica se estamos realizando testes. Algumas dependencias usam esta configuração para desativar funções como, por exemplo, o envio de emails.

```

Para facilitar a configuração podem ser adicionados os exports ao final do arquivo activate do ambiente virtual. Desta forma
sempre que o venv for ativado as variáveis serão automaticamente exportadas.

Um exemplo de configuração para um ambiente local seria:

```
export FLASK_ENV=development
export POSTGRES_URL=127.0.0.1
export POSTGRES_USER=dev_testeforall
export POSTGRES_PW=dev_testeforall
export POSTGRES_DB=dev_testeforall
export POSTGRES_PORT=5432
export APP_SETTINGS=config.py
export SK_APP=manager.py
export FLASK_APP=manager.py
export TASK_QUEUE=tasks
export UPLOAD_FOLDER=uploads
export EXPORT_FOLDER=export
export SERVER_NAME=localhost:5000
export PREFERRED_URL_SCHEME=http
export CELERY_BROKER_URL=redis://localhost:6379/0
export MAPS_API_KEY=no_free_api_today
export TESTING=True
```

Após definir estas configurações podemos ativar o ambiente virtual.

## Instalando as dependências do projeto

Para instalar as dependências do projeto basta rodar o comando:

```pip install -r requirements.txt```

Em alguns casos podem ocorrer erros ao instalar o psycopg2. Normalmente este erro é consertado ao rodar:

```sudo apt-get install libpq-dev python3-dev```

Em alguns casos será necessário rodar também: 

```
sudo apt-get install build-essential
sudo apt-get install postgresql-server-dev-all
sudo apt-get install build-essential python3-dev
```
_Fonte: [stackoverflow](https://stackoverflow.com/questions/5420789/how-to-install-psycopg2-with-pip-on-python)_

## Comandos da aplicação

Para facilitar o setup foram criados alguns comandos no flask que podem ser invocados pelo shell.
Estes comandos devem ser invocados da raiz do projeto a menos que seja explicitamente pedido para ir
a uma pasta específica

```flask create_db```

Inicializa toda estrutura do banco de dados

```flask drop_db```

Remove todas as tabelas do banco de dados

Os comandos acima não tem permissão para criar uma database, eles requerem uma já existente.

```flask run```

Roda o flask no servidor de desenvolvimento. Este servidor pode ser usado para o teste.

Para rodar o worker do celery é necessário usar o comando abaixo na raiz do projeto:

```celery -A celery_worker.celery_app worker --concurrency=1 -E -Q $TASK_QUEUE --loglevel=info```

O worker do celery deve estar rodando sempre que a aplicação estiver, inclusive nos testes.

### Rodando os testes

Para rodar os testes basta utilizar o comando. Caso não deseje usar uma base exclusiva para testes use o comando drop_db para limpar a base atual.
É necessário ter uma instância do celery rodando para os testes de integração. Pode ser usado o mesmo broker contanto com a TASK_QUEUE seja diferente.
O motivo da inicialização separada do celery é que o mesmo apresentava conflito com o flask-sqlalchemy ao ser iniciado em uma thread junto aos testes.
Para configurar o ambiente teste basta trocar os valores dos exports no arquivo activate, ou mesmo copiar o arquivo, alterar os exports 
e ativar o ambiente utilizando o arquivo correspondente ao ambiente que deseja.

```nose2 -v```

É possível habilitar um relatório html de coverage para os testes com o comando: 

```nose2 --with-coverage```

Após isto basta abrir o arquivo coverage_html_report/index.html em um navegador.

### Documentação de código

A documentação de código foi criada utilizando o sphinx. Para gerar os arquivos de 
documentação basta acessar a pasta docs rodar o comando:

```sphinx-apidoc -f -o . ../```

Após isto ainda na pasta docs rodar o comando:

```make html```

Após rodar este comando é possível acessar a documentação abrindo o arquivo docs/_build/html/index.html em um navegador.
Basta ir em module_contents e terá a documentação de todos os módulos.

### Documentação de api

A documentação de api foi criada utilizando o SwaggerUI. Ela está imbutida na aplicação.
Basta acessar as rotas /docs e /docs/editor para visualizá-la.

### Linters

O Linter utilizado no projeto foi o Pylama. Para rodar o pylama basta utiliza o comando.

```pylama arquivo ou pasta```

Normalmente pode-se rodar apenas pylama na raiz do projeto, mas eu encontrei problemas ao realizar desta forma.
E por questões de tempo não consegui resolver.

### Migrations

A aplicação tem suporte a migrations, utilizando a extensão flask-migrate. Para uma aplicação real utilizaríamos ela ao
invés dos comandos flask create_db e flask_drop_db.

Obs: Tive problema com os arquivos do exercício então criei arquivos de teste na pasta test/test-files.

