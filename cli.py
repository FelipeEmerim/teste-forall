"""Contain flask commands that are called from shell."""

from main import db


def register(app):
    """Register the application shell commands."""
    @app.cli.command('create_db')
    def init_db():  # noqa:W0612 pylint thinks it is an unused attribute
        """Create all sqlalchemy tables imported in the init.py of the main module."""
        db.create_all()

    @app.cli.command('drop_db')
    def drop_db():  # noqa:W0612 pylint thinks it is an unused attribute
        """Drop all sqlalchemy tables imported in the init.py of the main module."""
        db.drop_all()
