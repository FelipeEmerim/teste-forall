"""Contain post app initialization logic."""

import os
import traceback

from flask import jsonify
from werkzeug.exceptions import HTTPException, default_exceptions

from main import create_app
import cli


flask_app = create_app()
cli.register(flask_app)

if not os.path.exists(flask_app.config['UPLOAD_FOLDER']):
    os.mkdir(flask_app.config['UPLOAD_FOLDER'])

if not os.path.exists(flask_app.config['EXPORT_FOLDER']):
    os.mkdir(flask_app.config['EXPORT_FOLDER'])


@flask_app.errorhandler(Exception)
def handle_error(e):
    """Handle every application error."""
    flask_app.logger.error(traceback.format_exc())
    code = 500
    message = "internal server error"
    if isinstance(e, HTTPException):
        code = e.code
        message = e
    return jsonify({'success': False, 'message': str(message)}), code


for ex in default_exceptions:
    flask_app.register_error_handler(ex, handle_error)
