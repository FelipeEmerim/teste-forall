"""
Configuration for the flask application.

Configures the application and all of its modules, database connection and env var configs.
"""

import os


def get_env_variable(name):
    """
    Get the value of an env var that matches the given name.

    These are passed via environment for safety reasons.

    :param name: the name of the env var to get.
    :return: the value of the env var if it was found.
    """
    try:
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' not set.".format(name)
        raise Exception(message)


class Config:

    """
    Contain the application configs.

    Configs that change according to environment are passed via env vars.
    """

    UPLOAD_FOLDER = get_env_variable("UPLOAD_FOLDER")
    EXPORT_FOLDER = get_env_variable("EXPORT_FOLDER")
    TESTING = get_env_variable('TESTING')

    # postgres configs
    POSTGRES_URL = get_env_variable("POSTGRES_URL")
    POSTGRES_USER = get_env_variable("POSTGRES_USER")
    POSTGRES_PW = get_env_variable("POSTGRES_PW")
    POSTGRES_DB = get_env_variable("POSTGRES_DB")
    POSTGRES_PORT = get_env_variable("POSTGRES_PORT")

    # To turn on query logging set the config SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://{user}:{pw}@{url}:{port}/{db}'.format(user=POSTGRES_USER,
                                                                                           pw=POSTGRES_PW,
                                                                                           url=POSTGRES_URL,
                                                                                           db=POSTGRES_DB,
                                                                                           port=POSTGRES_PORT)
    # engine parameters
    SQLALCHEMY_ENGINE_OPTIONS = {
        'pool_pre_ping': True,
        'pool_size': 5,
        'pool_recycle': 7200,
        'max_overflow': 4,
        'echo_pool': True,
    }

    SQLALCHEMY_TRACK_MODIFICATIONS = False  # silence the deprecation warning

    SECRET_KEY = os.urandom(16)

    # celery
    CELERY_BROKER_URL = get_env_variable("CELERY_BROKER_URL")

    # process can only stay this time in the queue
    CELERY_TASK_SOFT_TIME_LIMIT = (3600*6)
    CELERY_LOG_COLOR = True
    TASK_QUEUE = get_env_variable('TASK_QUEUE')

    # maps api
    MAPS_API_KEY = get_env_variable('MAPS_API_KEY')

    # url generation
    PREFERRED_URL_SCHEME = get_env_variable('PREFERRED_URL_SCHEME')
    SERVER_NAME = get_env_variable('SERVER_NAME')
