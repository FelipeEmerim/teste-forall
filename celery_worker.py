"""Module that start celery workers with protection to disconnects and connection leaks."""

from celery.signals import worker_process_init

# noinspection PyUnresolvedReferences
from main import create_app, celery_app, db  # noqa:W0611 Celery app is used to start the celery worker

flask_app = create_app()


@worker_process_init.connect
def init_worker(**_):
    """
    Make every celery worker use a new engine object to prevent connection leaking.

    :param _: Not used

    :return: None
    """
    with flask_app.app_context():
        db.session.close()
        db.engine.dispose()
