"""Contain the business logic relates to address."""

import googlemaps
from flask import current_app
from sqlalchemy import and_

from config import Config
from main import db
from main.model.Address import Address, TempAddress
from main.service import fileService

gmaps = googlemaps.Client(key=Config.MAPS_API_KEY)


def find_addresses(file):
    """
    Try to find addresses for every coordinate in a coordinates file.

    :param file: The file to be processed

    :return: None
    """
    # Current job addresses are stored in a temp table, this is to separate them from the global address table
    # Which will contain all addresses ever found. Later when we export the addresses to a file, we will only
    # export this job's address and not every address ever found
    TempAddress.__table__.drop(db.session.connection(), checkfirst=True)
    TempAddress.__table__.create(db.session.connection())

    with open(file, 'r', newline='') as infile:

        for next_three_lines in fileService.iterate_n_lines_at_a_time(infile, 3):
            temp_address = find_address(lat=next_three_lines[0][22:], lng=next_three_lines[1][23:])
            db.session.add(temp_address)


def validate_coordinate(coordinate, row_num):
    """
    Run basic validation against a coordinate.

    Checks if a coordinate is a number and has appropriate length.

    :param coordinate: The coordinate to be validated
    :param row_num: The current row number. Helps logging.

    :return: None

    :raise: ValueError when the coordinates is not valid.
    """
    try:
        float(coordinate)

        if len(coordinate) not in [12, 13]:
            raise ValueError

    except ValueError:
        raise ValueError('Error in row {}: value {} is not a valid coordinate'.format(row_num, coordinate))


def find_address(lat, lng):
    """
    Find an address for a given latitude and longitude.

    :param lat: The latitude.
    :param lng: The longitude.

    :return: The address with details if they were found.
    """
    # Here we save money and time by only requesting google for an address when we dont have it
    address = get_address_from_db(lat, lng) or reverse_geocode(lat, lng)
    return address


def reverse_geocode(lat, lng):
    """
    Find an address details given its coordinates.

    :param lat: The latitude.
    :param lng: The longitude.

    :return: The address with details if found.
    """
    current_app.logger.info("Address not in database")
    address = Address(latitude=float(lat), longitude=float(lng))
    db.session.add(address)

    # exclude APPROXIMATE results since they are very low quality
    reverse_geocode_response = gmaps.reverse_geocode((address.latitude, address.longitude),
                                                     location_type=['ROOFTOP',
                                                                    'RANGE_INTERPOLATED',
                                                                    'GEOMETRIC_CENTER'],
                                                     result_type=['street_address', 'route']
                                                     )
    address = parse_reverse_geocode_response(address, reverse_geocode_response)

    temp_address = TempAddress()

    temp_address.copy_address(address)

    return temp_address


def parse_reverse_geocode_response(address, reverse_geocode_response):
    """
    Parse the gmaps api response.

    This will set the found attributes in the address object, not found attributes will be left blank.
    If the address is not found at all, it will be marked as such and no details will be put into it.

    :param address: The address object to insert the details
    :param reverse_geocode_response: The gmaps response list.

    :return: The updated address
    """
    # if nothing was found
    if not reverse_geocode_response:
        address.found = False
        return address

    address_types = address.get_address_types()

    # iterate over every possible attribute and try to find it in the gmaps response
    for address_type in address_types:
        type_value = get_type_value_from_request(address_type, reverse_geocode_response)
        address.set_attribute(address_type['type'], type_value)
        address.found = True

    return address


def get_type_value_from_request(address_type, reverse_geocode_response):
    """
    Get the value of an address type from the gmaps request.

    :param address_type: Can be street_number, route, country etc in format {type : '', property: ''}
    :param reverse_geocode_response: The gmaps response list.

    :return: The value of the address type or not if no value was found.
    """
    # Here we get the first item which is always the most precise
    address_components = reverse_geocode_response[0]['address_components']

    value = None

    # try to find the attribute and return the desired description
    for address_component in address_components:
        if address_type['type'] not in address_component['types']:
            continue

        value = address_component[address_type['property']]

    return value


def get_address_from_db(lat, lng):
    """
    Return an address from the database given its coordinates.

    :param lat: The latitude
    :param lng: The longitude

    :return: The sqlalchemy object containing the address or None in case it was not found.
    """
    address = Address.query.filter(and_(Address.latitude == lat, Address.longitude == lng)).first()

    if not address:
        return None

    temp_address = TempAddress()
    temp_address.copy_address(address)
    return temp_address
