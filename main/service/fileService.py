"""Contain business logic related to files."""

import os
import uuid
import zipfile
from itertools import islice

import magic
from flask import current_app
from sqlalchemy import and_

from main import db
from main.model.Address import TempAddress
from main.model.AddressFile import AddressFile
from main.service import addressService


def save_file(file):
    """
    Given a file handler save it to the application upload folder.

    If no file was sent or the filename is empty, a value error is raised.
    If the file was sent, the application generates a unique name for the file
    and saves it into the upload folder. This is to avoid file overwriting since the upload folder is shared by
    all tenants and multiple files can be saved at the same time. At this point the file is not validated,
    validation will occur in the address finder job.

    :param file: A werkzeug.FileStorage object containing the file

    :return: The unique name of the saved file.
    """
    # checks if the file part is empty
    if file.filename == '':
        raise ValueError("Cannot process empty file")

    # generates unique name for the file while keeping the extension, this avoids file overwriting
    filename = str(uuid.uuid4()) + '.' + file.filename.rsplit('.')[-1].lower()

    # The file needs to be saved so that we can access it in celery
    file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))

    return filename


def handle_zip_files(zfile, file_address, folder):
    """
    Unzip a file, validates both the zip and its contents.

    In case the file is invalid its is automatically deleted from the filesystem.
    If the zipfile is valid its content is analysed. If the content is valid then it is saved with a
    unique name to the application's upload folder to avoid overwriting.

    :param zfile: the zipfile object.
    :param file_address: the address of the zipfile.
    :param folder: the folder to work in.

    :returns: The address of the valid file or None if the file is not valid.
    """
    # checks if the zipfile contains more than one file
    if len(zfile.namelist()) > 1:
        os.remove(file_address)
        return None

    # gets the name of the file within the zipfile
    filename = zfile.namelist()[0]

    # generates a unique file name to avoid overwriting while keeping the extension
    textfile_name = str(uuid.uuid4()) + filename.rsplit('.')[-1].lower()
    textfile = os.path.join(folder, textfile_name)

    # creates the file to receive the content
    with open(textfile, 'wb') as file:
        file.write(zfile.read(filename))
    os.remove(file_address)

    # validates the file within the zip file
    return process_file(textfile_name, folder)


def process_file(filename, folder):
    """
    Validate any file, if the file is a zipfile it calls a method to handle it.

    If the file is not valid the request is aborted and the file is automatically deleted.

    :param filename: The name of the file to be validated.
    :param folder: the server's upload folder.

    :return: the address of the validated file or None in case of errors.
    """
    file_address = os.path.join(folder, filename)

    # checks if the file is plain text and with the csv extension
    if magic.from_file(file_address, mime=True) == 'text/plain':
        return file_address

    # checks if the file is a valid zipfile
    if zipfile.is_zipfile(file_address) and magic.from_file(file_address, mime=True) == 'application/zip':

        # open the zipfile in a context manager, it closes the file automatically
        with zipfile.ZipFile(file_address) as zfile:

            # handles the zipfile
            file_address = handle_zip_files(zfile, file_address, folder)

            if file_address:
                return file_address

            return None

    os.remove(file_address)
    return None


def validate_file(file):
    """
    Run basic validation on all rows of the coordinates file.

    :param file: The file to be validated

    :return: None

    :raise: ValueError if the file is not valid.
    """
    with open(file, 'r', newline='') as infile:
        row_num = 0

        for line_group in iterate_n_lines_at_a_time(infile, n=3):

            # Keep track of the current row
            row_num += len(line_group)

            # Since we are working with fixed width files these positions wont change
            addressService.validate_coordinate(line_group[0][22:], row_num - 2)   # latitude
            addressService.validate_coordinate(line_group[1][23:], row_num - 1)   # longitude

        if row_num % 3 != 0:
            raise ValueError('File line count is not a multiple of three')


def iterate_n_lines_at_a_time(file, n=3):
    """
    Get n rows from a file at a time. When there are less rows than n, return those rows only.

    :param file: The file to be processed, must be opened beforehand
    :param n: The number of rows to iterate at a time
    :return: A list containing the n rows
    """
    while True:
        next_n_lines = read_next_n_lines(file, n)

        # End of file
        if not next_n_lines:
            break
        yield strip_newlines(next_n_lines)


def strip_newlines(lines=None):
    """
    Remove newlines from a group of lines.

    Newlines are bad for the validation.

    :param lines: The group of lines to be processed,

    :return: The lines without newline characters.
    """
    if lines is None:
        lines = []

    return [line.strip(os.linesep) for line in lines]


def read_next_n_lines(file, n=1):
    """
    Get next n lines of a file.

    :param file: The file to be processed
    :param n: The number of lines to get.

    :return: A list containing the n lines.
    """
    return list(islice(file, n))


def count_rows(file):
    """
    Count the rows in a file.

    :param file: The file to be processed, must be opened beforehand.

    :return: the number of rows in the file.
    """
    rows_count = sum(1 for _ in file)
    # Return the file pointer to the start of the file
    file.seek(0)
    return rows_count


def export_address_file(found, job, include_columns, folder=None, encoding='utf-8'):
    """
    Export an address file for a job.

    Export the temp_address table into a file. Can export either found or not found addresses.

    :param found: Indicates if the file will contain found or not found addresses
    :param job: The current running job
    :param include_columns: The columns to export
    :param folder: The app export folder
    :param encoding: The encoding of the export file

    :return: None
    """
    if folder is None:
        folder = current_app.config['EXPORT_FOLDER']

    # generate unique name for file
    filename = uuid.uuid4().hex + '.csv'
    file_address = os.path.join(folder, filename)

    # build columns string
    columns = ['"{}"'.format(c) for c in include_columns]
    columns_string = ', '.join(columns)

    file = AddressFile(job_id=job.id, filename=filename, directory=folder, is_success=found)
    db.session.add(file)

    # Create query using sqlalchemy names, if we change column or table names dont need to change it here too.
    # Since there is no user supplied data we can use python format.
    query = '''
            COPY
                (SELECT {columns} FROM "{table}" WHERE "{found}" = {value})
            TO STDOUT
            WITH(FORMAT CSV, HEADER, DELIMITER ';')'''.format(columns=columns_string,
                                                              table=TempAddress.__tablename__,
                                                              found=TempAddress.found.name,
                                                              value=found)

    # Make sure data is available to copy command
    db.session.flush()

    # sqlalchemy does not support postgres copy, here we use psycopg2 which is running under sqlalchemy
    # to use the copy command. The file needs to be created before copying because copy can only create a file by
    # itself when running as root.
    with open(file_address, 'w', newline='', encoding=encoding) as file:
        cursor = db.session.connection().connection.cursor()
        cursor.copy_expert(query, file)


def find_by_is_success_and_job_id(is_success, job_id):
    """
    Find a file object by success and by job.

    :param is_success: Indicates if the file contains found or not found addresses
    :param job_id: The id of the job that the file is related to

    :return: A sqlalchemy object containing the file.
    """
    return AddressFile.query.filter(
        and_(AddressFile.is_success.is_(is_success), AddressFile.job_id == job_id))\
        .first()


def remove_file(file):
    """
    Remove a file given a file address.

    :param file: The file to remove.

    :return: None
    """
    os.remove(file)
