"""Contain the business logic related to jobs, it is also the module that access the database."""

import traceback
from datetime import datetime
from gettext import find

from flask import current_app
from sqlalchemy import exc

from config import Config
from main import db, celery_app
from main.model.Address import TempAddress
from main.model.Job import Job
from main.service import fileService, addressService


def create_job(file):
    """
    Create an address finder job.

    :param file: The file that contains the coordinates.
    :return: The job that was created
    """
    job = Job()
    job.init(name='Address finder',
             description="Converts coordinates into addresses",
             message='job created')

    db.session.add(job)
    db.session.commit()

    # process the request file and save it to the upload folder
    filename = fileService.save_file(file)

    job = find_by_id(job.id)

    # put a celery task on the queue, this will run in background
    address_finder_task.delay(job.id, filename, current_app.config['UPLOAD_FOLDER'])

    return job


def finish_job(job, status, message, finish_date=datetime.now()):
    """

    Finish an address finder job.

    Helper method was necessary because this was a very repetitive operation.

    :param job: The job to be finished
    :param status: The status the job will take
    :param message: The message the job will take
    :param finish_date: The date the job was finished. Defaults to now
    :return: None
    """
    job.status = status
    job.message = message
    job.finished_on = finish_date
    db.session.commit()


def find_by_id(job_id):
    """
    Find a job given a job id.

    :param job_id: The id of the job to be found.

    :return: The job related to the given id
    """
    return Job.query.filter_by(id=job_id).first()


# The only case the task will ever retry is if we lose connection to the db during processing
# in which case it will not be able to update the status of the job while handling the exception
# This is because we are handling and logging every exception within the job
@celery_app.task(queue=Config.TASK_QUEUE, autoretry_for=(exc.SQLAlchemyError,),
                 retry_kwargs={'max_retries': 2, 'countdown': 60})
def address_finder_task(job_id, file, folder):  # pragma: no cover
    """
    Celery task of the address finder job.

    It is separated like this so that we actually do not need celery running to run the tests.
    The actual function is the address_finder

    :param job_id: The id of the job that will run
    :param file: The file containing the coordinates
    :param folder: The app uploads folder

    :return: None
    """
    current_app.logger.info("ADDRESS FINDER TASK INVOKED")
    address_finder(job_id, file, folder)


def address_finder(job_id, file, folder):
    """

    Validate a file get coordinates from a it, convert into address and export the results.

    This will call all functions need from other services and is responsible for keeping track of
    the job.

    :param job_id: The job that is running.
    :param file: The file containing the coordinates.
    :param folder: The app uploads folder.

    :return: None
    """
    job = find_by_id(job_id)

    job.status = 1
    db.session.commit()

    # Here some basic validation is done to the file, in case it is zipped it is unzipped
    # and the content is run against the basic validation
    textfile = fileService.process_file(file, folder)

    if textfile is None:
        finish_job(job, 2, 'Invalid file format supplied')
        return

    try:
        # Here we validate the content of the file
        fileService.validate_file(textfile)

        addressService.find_addresses(textfile)
        finish_success_job(job)
    # should only enter here in case of invalid file content
    except ValueError as e:
        db.session.rollback()
        current_app.logger.error(traceback.format_exc())
        finish_job(job, status=2, message=str(e))
    except Exception as e:  # noqa:W0703 Catches all exceptions but still logs them and emphasizes the error status.
        db.session.rollback()
        current_app.logger.error(traceback.format_exc())
        current_app.logger.error(e)
        finish_job(job, status=2, message='An error occurred during file processing')
    finally:

        db.session.commit()
        fileService.remove_file(textfile)


def finish_success_job(job):
    """

    Finish a job that successfully ran. Also export the address files.

    A job is considered successful when no errors occurred during the execution.

    :param job: The job to be finished

    :return: None
    """
    # export the file containing the found addresses
    fileService.export_address_file(found=True, job=job, include_columns=TempAddress.get_export_columns())
    status = 3
    message = 'job finished successfully'

    # In case some addresses where not found a file is exported with them
    # The job gets the status finished with errors
    if TempAddress.query.filter(TempAddress.found.is_(False)).first():
        fileService.export_address_file(found=False, job=job, include_columns=TempAddress.get_export_columns())
        message = 'Some addresses could not be found'
        status = 4

    finish_job(job, status, message)


def find_all():
    """
    Get all jobs from the database.

    :return: A list of sqlalchemy objects.
    """
    return Job.query.all()
