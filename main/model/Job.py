"""Contain models related to job."""

from sqlalchemy import func

from main import db


class Job(db.Model):

    """Define the columns and constraints of the job table."""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    description = db.Column(db.String())
    message = db.Column(db.String())
    created_on = db.Column(db.DateTime(timezone=True), default=func.now())
    finished_on = db.Column(db.DateTime(timezone=True), default=None)
    status = db.Column(db.Integer, default=0)

    @property
    def str_created_on(self):
        """Format date, will be used to display as json."""
        return self.created_on.strftime("%Y-%m-%d, %H:%M:%S")

    @property
    def str_finished_on(self):
        """Format date, will be used to display as json."""
        return self.finished_on.strftime("%Y-%m-%d, %H:%M:%S")

    @property
    def str_status(self):
        """Map a status number to a more meaningful text, will be used to display as json."""
        status_list = [
            'CREATED', 'IN PROGRESS', 'ERROR', 'FINISHED', 'FINISHED WITH ERRORS'
        ]

        return status_list[self.status]

    def init(self, name, description, message):
        """Job model custom constructor, initializes only a few parameters."""
        self.name = name
        self.description = description
        self.message = message
