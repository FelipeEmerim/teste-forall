"""Contain models related to Address file."""

from sqlalchemy import ForeignKey

from main import db


class AddressFile(db.Model):

    """Represent the address file table in the application."""

    id = db.Column(db.BigInteger, primary_key=True)
    job_id = db.Column(db.BigInteger,
                       ForeignKey('job.id', ondelete='CASCADE', name="fk_address_file_job_id"), nullable=False)
    filename = db.Column(db.String, nullable=False)
    directory = db.Column(db.String, nullable=False)
    is_success = db.Column(db.Boolean, default=True)

    def __init__(self, job_id, filename, directory, is_success):
        """Address file model constructor."""
        self.job_id = job_id
        self.filename = filename
        self.directory = directory
        self.is_success = is_success
