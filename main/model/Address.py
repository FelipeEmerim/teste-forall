"""Contains models related to address."""
from sqlalchemy import Index

from main import db


class _Address:  # noqa:R0902 This has more than 7 attributes because it maps a table

    """Contains common address attributes. Will not create a table because it does not extends db.Model."""

    id = db.Column(db.Integer, primary_key=True)
    latitude = db.Column(db.Numeric)
    longitude = db.Column(db.Numeric)
    found = db.Column(db.Boolean)
    street_number = db.Column(db.String)
    street_address = db.Column(db.String)
    neighborhood = db.Column(db.String)
    city = db.Column(db.String)
    state = db.Column(db.String)
    country = db.Column(db.String)
    postal_code = db.Column(db.String)

    @staticmethod
    def _address_mapper(address_type):
        """
        Convert a gmaps response address type into one of the model's columns.

        :param address_type: The gmaps address type

        :return: The model column as string
        """
        mapper = {
            'street_number': 'street_number',
            'route': 'street_address',
            'sublocality_level_1': 'neighborhood',
            'administrative_area_level_2': 'city',
            'administrative_area_level_1': 'state',
            'country': 'country',
            'postal_code': 'postal_code',
        }

        return mapper[address_type]

    @staticmethod
    def get_address_types():
        """
        Get the address types we want from the gmaps response as well as the property from said type we want.

        :return: A dict containing the type and its property
        """
        return [
            {'type': 'street_number', 'property': 'long_name'},
            {'type': 'route', 'property': 'long_name'},
            {'type': 'sublocality_level_1', 'property': 'long_name'},
            {'type': 'administrative_area_level_2', 'property': 'long_name'},
            {'type': 'administrative_area_level_1', 'property': 'short_name'},
            {'type': 'country', 'property': 'long_name'},
            {'type': 'postal_code', 'property': 'long_name'}
        ]

    def set_attribute(self, attribute, value):
        """
        Set a class attribute given a gmaps attribute.

        :param attribute: The gmaps attribute description.
        :param value: The gmaps attribute value

        :return: None
        """
        setattr(self, self._address_mapper(attribute), value)


class TempAddress(db.Model, _Address):  # noqa:R0902 This has more than 7 attributes because it maps a table

    """Temporary table that will contain jobs addresses."""

    # This makes the table temporary
    __table_args__ = {'prefixes': ['TEMPORARY']}

    def copy_address(self, address):
        """
        Copy the values of a global address object to this object.

        :param address: The global address object.

        :return: None
        """
        self.latitude = address.latitude
        self.longitude = address.longitude
        self.found = address.found
        self.street_number = address.street_number
        self.street_address = address.street_address
        self.neighborhood = address.neighborhood
        self.city = address.city
        self.state = address.state
        self.country = address.country
        self.postal_code = address.postal_code

    @classmethod
    def get_export_columns(cls):
        """
        Get the columns we want to export in the address files.

        This is needed not only to make the result customizable but to ensure column order.

        :return: A list containing this model's columns.
        """
        return [
            cls.latitude.name,
            cls.longitude.name,
            cls.street_address.name,
            cls.street_number.name,
            cls.neighborhood.name,
            cls.city.name,
            cls.postal_code.name,
            cls.state.name,
            cls.country.name,

        ]


class Address(db.Model, _Address):  # noqa:R0902 This has more than 7 attributes because it maps a table

    """
    Maps the global address table, this will keep every address we found.

    This makes the process run faster and reduces costs.
    """

    def __init__(self, longitude, latitude):
        """Address model constructor."""
        self.latitude = latitude
        self.longitude = longitude


Index('address_lat_lng_index', Address.latitude, Address.longitude)
