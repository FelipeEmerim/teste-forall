"""Contain marshmallow schemas related to job."""

from flask_marshmallow.fields import fields
from marshmallow import post_dump

from main import ma
from main.model.Job import Job


class JobSchema(ma.ModelSchema):

    """Json representation of the job model."""

    model = Job

    status = fields.String(attribute='str_status')
    created_on = fields.String(attribute='str_created_on')
    finished_on = fields.String(attribute='str_finished_on')

    class Meta:

        """Contain the fields of the job model that will be displayed."""

        fields = ('id', 'name', 'description', 'message', 'created_on', 'finished_on', 'status', '_links')

    _links = ma.Hyperlinks(
        {
            'self': ma.AbsoluteURLFor('jobBlueprint.job_details', job_id='<id>'),
            'new': ma.AbsoluteURLFor('jobBlueprint.create_job'),
            'collection': ma.AbsoluteURLFor('jobBlueprint.jobs_details'),
            'success_file': ma.AbsoluteURLFor('fileBlueprint.get_job_file', job_id='<id>', is_success='true'),
            'error_file': ma.AbsoluteURLFor('fileBlueprint.get_job_file', job_id='<id>')
        }
    )

    @post_dump
    def postprocess(self, data):  # noqa:R0201 Could be a function but marshmallow defines it this way
        """
        Run after schema.dump.

        Here we remove conditional attributes if necessary.

        :param data: The data that was dumped.

        :return: None
        """
        if data['status'] != 'FINISHED WITH ERRORS':
            del data['_links']['error_file']

            if data['status'] != 'FINISHED':
                del data['_links']['success_file']


job_schema = JobSchema()
jobs_schema = JobSchema(many=True)
