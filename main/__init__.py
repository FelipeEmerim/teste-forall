"""The heart of the application, defines the create_app and initialize all extensions."""

from celery import Celery
from flask import Flask
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from swagger_ui import flask_api_doc

from config import Config

migrate = Migrate()
celery_app = Celery(__name__, broker=Config.CELERY_BROKER_URL)
db = SQLAlchemy()
ma = Marshmallow()


def create_app():
    """
    Create an instance of the flask app.

    Create the flask app, configure the logger, bind extensions to app object,
    push app context to celery tasks and register all blueprints.

    :return: An instance of Flask
    """
    app = Flask(__name__)
    app.config.from_object(Config)

    celery_app.conf.update(app.config)

    TaskBase = celery_app.Task

    class ContextTask(TaskBase):

        """Celery class with app context pushed to tasks."""

        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    # noinspection PyPropertyAccess
    celery_app.Task = ContextTask

    # Sqlalchemy models need to be imported before binding it with the current app
    # Otherwise sqlalchemy create and drop all wont work, migrations wont work

    from main.model.Job import Job  # noqa:W0611 necessary import
    from main.model.Address import Address  # noqa:W0611 necessary import
    from main.model.AddressFile import AddressFile  # noqa:W0611 necessary import

    # These imports are here to avoid circular import, it is also the only way to register blueprints
    # using the factory pattern
    from main.blueprint.jobBlueprint import jobBlueprint
    app.register_blueprint(jobBlueprint, url_prefix='/job')

    from main.blueprint.fileBlueprint import fileBlueprint
    app.register_blueprint(fileBlueprint, url_prefix='/file')

    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)
    flask_api_doc(app, config_path='./main/swagger.yml', url_prefix='/docs', title='API doc', editor=False)

    return app
