"""Contain routes related to job. Should not contain business logic."""

from flask import Blueprint, request, jsonify, url_for, abort

from main.schema.jobSchema import job_schema, jobs_schema
from main.service import jobService

jobBlueprint = Blueprint('jobBlueprint', 'jobBlueprint', url_prefix='/job')


@jobBlueprint.route("/create", methods=['POST'])
def create_job():
    """
    Create a new address finder job. More details on /docs.

    :return: A json object.
    """
    file = request.files['file']

    job = jobService.create_job(file)

    return jsonify({"success": True,
                    "message": "Job created successfully",
                    "status-link": url_for('jobBlueprint.job_details', job_id=job.id, _external=True)}), 201


@jobBlueprint.route('/<int:job_id>', methods=['GET'])
def job_details(job_id):
    """
    Details on: /docs.

    :param job_id:
    :return:
    """
    job = jobService.find_by_id(job_id)

    if not job:
        abort(404)

    return job_schema.jsonify(job)


@jobBlueprint.route('/', methods=['GET'])
def jobs_details():
    """
    Details on: /docs.

    :return:
    """
    return jobs_schema.jsonify(jobService.find_all())
