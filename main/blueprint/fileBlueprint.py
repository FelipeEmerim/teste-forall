"""Contain routes related to file. Should not contain business logic."""

import os

from flask import Blueprint, request, send_from_directory, abort

from main.service import fileService

fileBlueprint = Blueprint('fileBlueprint', 'fileBlueprint', url_prefix='/file')


@fileBlueprint.route('/job/<int:job_id>', methods=['GET'])
def get_job_file(job_id):
    """
    Details on /docs.

    :param job_id:
    :return:
    """
    is_success = request.args.get('is_success', default=False, type=bool)
    file = fileService.find_by_is_success_and_job_id(is_success, job_id)

    if not file:
        abort(404)

    root = os.getcwd()

    return send_from_directory(os.path.join(root, file.directory), file.filename, as_attachment=True)
